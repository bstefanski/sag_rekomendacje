#!/bin/bash

AGENT_CLASS=sag.rekomendacje.DispatcherAgent

mvn compile exec:java \
    -Dexec.mainClass=jade.Boot \
    -Dexec.args="-container -host localhost -agents dispatcher:sag.rekomendacje.DispatcherAgent"
