package sag.rekomendacje;

import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProfilingAgent extends Agent {

    private Recommendations recommendations;
    private History history = null;
    private DateTime lastActivityTime;

    @Override
    protected void setup() {
        registerAgent();
        initRecommendations();
        lastActivityTime = DateTime.now();
        addBehaviour(new ReceiveRequestBehaviour());
        addBehaviour(new RespondToNeighbourBehaviour());
        addBehaviour(new KillIdleAgentBehaviour(this));
    }

    @Override
    protected void takeDown() {
        deregisterAgent();
    }

    private void initRecommendations() {
        if (recommendations != null) {
            throw new RuntimeException("initRecommendations can be invoke only once");
        }
        recommendations = new Recommendations();
        recommendations.put(Lists.newArrayList(
            new Recommendation("The Lord of the Rings"),
            new Recommendation("Pride and Prejudice"),
            new Recommendation("His Dark Materials"),
            new Recommendation("The Hitchhiker's Guide to the Galaxy"),
            new Recommendation("Harry Potter and the Goblet of Fire"),
            new Recommendation("To Kill a Mockingbird"),
            new Recommendation("Winnie the Pooh"),
            new Recommendation("Nineteen Eighty-Four"),
            new Recommendation("The Lion, the Witch and the Wardrobe"),
            new Recommendation("Jane Eyre"),
            new Recommendation("Catch-22")));

        recommendations.put(Lists.newArrayList(
            new Recommendation("bialy"),
            new Recommendation("czarny"),
            new Recommendation("czerwony"),
            new Recommendation("zielony"),
            new Recommendation("zolty"),
            new Recommendation("niebieski"),
            new Recommendation("brazowy"),
            new Recommendation("pomaranczowy"),
            new Recommendation("rozowy"),
            new Recommendation("fioletowy"),
            new Recommendation("szary")));
    }

    private void registerAgent() {
        String localization = getArguments()[0].toString();
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("profiling-agent-" + localization);
        sd.setName("Recommendations");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }

    private void deregisterAgent() {
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }

    private class ReceiveRequestBehaviour extends CyclicBehaviour {
        private String request;
        private String localization;

        @Override
        public void action() {
            ACLMessage msg = receiveRecommendationRequest();
            if (msg != null) {
                lastActivityTime = DateTime.now();
                request = parseRequest(msg).get(0);
                localization = parseRequest(msg).get(1);
                checkIfPreviousRecommendationUsed();
                printRecommendationState();
                myAgent.addBehaviour(new GetRecommendationBehaviour(request, localization, msg));
            } else {
                block();
            }
        }

        private ACLMessage receiveRecommendationRequest() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
            return myAgent.receive(mt);
        }

        private List<String> parseRequest(ACLMessage msg) {
            String content = msg.getContent();
            Iterable<String> split = Splitter.on(';').split(content);
            ArrayList<String> strings = Lists.newArrayList(split);
            if (strings.size() != 2) {
                throw new RuntimeException("wrong number of received arguments");
            }
            return strings;
        }

        private void checkIfPreviousRecommendationUsed() {
            if (history != null && history.getRecommendations().contains(new Recommendation(request))) {
                Recommendation rec = Iterables.find(recommendations.get(history.getKey()), new Predicate<Recommendation>() {
                    public boolean apply(Recommendation r) {
                        return r.getVal().equals(request);
                    }
                });
                rec.amplify();
            }
        }

        private void printRecommendationState() {
            System.out.println(myAgent.getAID().getName() + " stan:");
            System.out.println(recommendations.toString());
        }
    }

    class GetRecommendationBehaviour extends Behaviour {
        private String request;
        private String localization;
        private ACLMessage receivedMsg;
        private CalculationState state;
        private List<AID> neighbors;
        private List<Recommendation> neighborsRecommendations;
        private MessageTemplate msgTemplate;
        private int repliesCnt;

        private GetRecommendationBehaviour(String request, String localization, ACLMessage receivedMsg) {
            super();
            this.request = request;
            this.localization = localization;
            this.receivedMsg = receivedMsg;
            repliesCnt = 0;
            neighborsRecommendations = Lists.newArrayList();
            state = CalculationState.ASKING;
        }

        @Override
        public void action() {
            switch (state) {
                case ASKING:
                    neighbors = findAgentsFromSameLocalization();
                    if (neighbors.isEmpty()) {
                        state = CalculationState.RECEIVED;
                        System.out.println(myAgent.getAID().getName() + " nie odpytal sasiadow.");
                    } else {
                        askNeighbors();
                        state = CalculationState.RECEIVING;
                        System.out.println(myAgent.getAID().getName() + " odpytal " + neighbors.size() + " sasiadow.");
                    }

                    break;
                case RECEIVING:
                    neighborsRecommendations.addAll(receiveRecommendationsFromNeighbors());
                    if (repliesCnt >= neighbors.size()) {
                        state = CalculationState.RECEIVED;
                        System.out.println(myAgent.getAID().getName() + " pobral " + neighborsRecommendations.size()
                            + " rekomendacje od sasiadow");
                        for (Recommendation r : neighborsRecommendations) {
                            System.out.println("\t" + r.toString());
                        }
                    }
                    break;
                case RECEIVED:
                    List<Recommendation> recommendations = calculateRecommendations();
                    System.out.println("Obliczone rekomendacje:");
                    for (Recommendation r : recommendations) {
                        System.out.println("\t" + r.toString());
                    }
                    ACLMessage reply = prepareRecommendationResponse(receivedMsg, recommendations);
                    myAgent.send(reply);
                    state = CalculationState.DONE;
                    break;
            }
        }

        @Override
        public boolean done() {
            return state == CalculationState.DONE;
        }

        private List<AID> findAgentsFromSameLocalization() {
            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType("profiling-agent-" + localization);
            template.addServices(sd);
            List<AID> agents = Lists.newArrayList();
            try {
                DFAgentDescription[] result = DFService.search(myAgent, template);
                for (DFAgentDescription agentDescription : result) {
                    if (!agentDescription.getName().equals(ProfilingAgent.this.getAID())) {
                        agents.add(agentDescription.getName());
                    }
                }
            } catch (FIPAException fe) {
                fe.printStackTrace();
            }
            return agents;
        }


        private void askNeighbors() {
            ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
            for (AID neighbor : neighbors) {
                cfp.addReceiver(neighbor);
            }
            cfp.setContent(request);
            cfp.setConversationId("ask-neighbors");
            cfp.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
            myAgent.send(cfp);
            // Prepare the template to get proposals
            msgTemplate = MessageTemplate.and(MessageTemplate.MatchConversationId("ask-neighbors"),
                MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
        }

        private List<Recommendation> receiveRecommendationsFromNeighbors() {
            List<Recommendation> nRecommendations = Lists.newArrayList();
            ACLMessage reply = myAgent.receive(msgTemplate);
            if (reply != null) {
                if (reply.getPerformative() == ACLMessage.PROPOSE) {
                    try {
                        nRecommendations = (List<Recommendation>) reply.getContentObject();
                    } catch (UnreadableException e) {
                        e.printStackTrace();
                    }
                }
                repliesCnt++;
            } else {
                block();
            }
            return nRecommendations;
        }

        private List<Recommendation> calculateRecommendations() {
            return new RecommendationCalculator(request, recommendations.get(request), neighborsRecommendations).calculate();
        }

        private ACLMessage prepareRecommendationResponse(ACLMessage msg, List<Recommendation> recommendations) {
            if (recommendations != null && !recommendations.isEmpty()) {
                history = new History(request, recommendations);
            } else {
                history = null;
            }
            ACLMessage reply = msg.createReply();
            if (recommendations != null && !recommendations.isEmpty()) {
                reply.setPerformative(ACLMessage.PROPOSE);
                List<String> recVals = Lists.newArrayList();
                for (Recommendation r : recommendations) {
                    recVals.add(r.getVal());
                }
                reply.setContent(Joiner.on(", ").skipNulls().join(recVals));
                System.out.println(myAgent.getAID().getName() + " wyslal rekomendacje");
            } else {
                reply.setPerformative(ACLMessage.REFUSE);
                reply.setContent("no-recommendations");
                System.out.println(myAgent.getAID().getName() + " wyslal pusta odpowiedz");
            }
            return reply;
        }
    }

    private class RespondToNeighbourBehaviour extends CyclicBehaviour {
        @Override
        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
            ACLMessage msg = myAgent.receive(mt);
            if (msg != null) {
                lastActivityTime = DateTime.now();
                String request = msg.getContent();
                ACLMessage reply = msg.createReply();
                if (recommendations.contains(request)) {
                    reply.setPerformative(ACLMessage.PROPOSE);
                    try {
                        reply.setContentObject((ArrayList) recommendations.get(request));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    reply.setPerformative(ACLMessage.REFUSE);
                    reply.setContent("not-available");
                }
                myAgent.send(reply);
            } else {
                block();
            }
        }
    }

    private class KillIdleAgentBehaviour extends TickerBehaviour {

        public KillIdleAgentBehaviour(Agent a) {
            super(a, 30 * 1000); // 30 seconds
        }

        @Override
        protected void onTick() {
            if (lastActivityTime.plusMinutes(10).isBeforeNow()) {
                myAgent.doDelete();
            }
        }
    }

    private enum CalculationState {
        ASKING, RECEIVING, RECEIVED, DONE
    }
}
