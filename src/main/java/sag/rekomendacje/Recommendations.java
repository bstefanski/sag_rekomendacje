package sag.rekomendacje;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Bartlomiej Stefanski
 */
public class Recommendations {
    List<List<Recommendation>> groups = Lists.newArrayList();

    public void put(ArrayList<Recommendation> recommendations) {
        groups.add(recommendations);
    }

    public List<Recommendation> get(String key) {
        for (List<Recommendation> group : groups) {
            if (group.contains(new Recommendation(key))) {
                return group;
            }
        }
        return null;
    }

    public boolean contains(String key) {
        return get(key) != null;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (List<Recommendation> group : groups) {
            str.append("[\n");
            for (Recommendation r : group) {
                str.append("\t" + r.toString() + "\n");
            }
            str.append("]\n");
        }
        return str.toString();
    }
}
