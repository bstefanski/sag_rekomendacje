package sag.rekomendacje;

import com.google.common.collect.Lists;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.AgentController;
import jade.wrapper.PlatformController;

import java.util.List;

public class DispatcherAgent extends Agent {
    private DispatcherGui myGui;

    @Override
    protected void setup() {
        createGui();
        registerAgentInDF();
    }

    private void createGui() {
        myGui = new DispatcherGui(this);
        myGui.display();
    }

    private void registerAgentInDF() {
        //rejestracja sprzedazy w katalogu df
        DFAgentDescription agentDescription = getAgentDescription();
        try {
            DFService.register(this, agentDescription);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }

    private DFAgentDescription getAgentDescription() {
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("profiling-dispatcher");
        sd.setName("JADE-profiling-dispatcher");
        dfd.addServices(sd);
        return dfd;
    }

    @Override
    protected void takeDown() {
        deregisterFromDF();
        cleanGui();
        System.out.println(getAID().getName() + " zakonczyl dzialanie.");
    }

    private void deregisterFromDF() {
        //wyrejestrowanie agenta z katalogu df
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }

    private void cleanGui() {
        myGui.dispose();
    }

    public void handleClientRequest(final String clientId, final String request, final String localisation) {
        ProfilingRequest profilingRequest = new ProfilingRequest(clientId, request, localisation);
        addBehaviour(new DispatchingBehaviour(this, profilingRequest));
    }

    /**
     * User: Bartlomiej Stefanski
     */
    static class DispatchingBehaviour extends Behaviour {
        private final ProfilingRequest profilingRequest;
        private DispatcherAgent dispatcherAgent;
        private State state;
        private MessageTemplate messageTemplate;

        DispatchingBehaviour(DispatcherAgent dispatcherAgent, ProfilingRequest request) {
            this.dispatcherAgent = dispatcherAgent;
            this.profilingRequest = request;
            this.state = State.SENDING;
        }

        @Override
        public void action() {
            if (state == State.SENDING) {
                System.out.println("\n\n" + dispatcherAgent.getAID().getName() + " przyjal request: " +
                    profilingRequest.getRequest());
                AID profilingAgentId = getAssignedProfilingAgentId();
                if (profilingAgentId != null) {
                    sendRequestMessage(profilingAgentId);
                }
            } else if (state == State.RECEIVING) {
                receiveResponseMessage();
            }
        }

        private AID getAssignedProfilingAgentId() {
            AID profilingAgentId;
            if (isRequestHasAssignedProfilingAgent()) {
                profilingAgentId = findAssignedProfilingAgent();
            } else {
                profilingAgentId = createNewProfilingAgent();
            }
            return profilingAgentId;
        }

        private boolean isRequestHasAssignedProfilingAgent() {
            AID name = new AID("profiling-agent-" + profilingRequest.getClientId(), AID.ISLOCALNAME);
            DFAgentDescription template = new DFAgentDescription();
            template.setName(name);
            DFAgentDescription[] result = {};
            try {
                result = DFService.search(myAgent, template);
            } catch (FIPAException fe) {
                fe.printStackTrace();
            }
            return result.length != 0;
        }

        private AID findAssignedProfilingAgent() {
            AID name = new AID("profiling-agent-" + profilingRequest.getClientId(), AID.ISLOCALNAME);
            DFAgentDescription template = new DFAgentDescription();
            template.setName(name);
            List<AID> agents = Lists.newArrayList();
            try {
                DFAgentDescription[] result = DFService.search(myAgent, template);
                for (DFAgentDescription agentDescription : result) {
                    agents.add(agentDescription.getName());
                }
            } catch (FIPAException fe) {
                fe.printStackTrace();
            }

            if (agents.size() != 1) {
                throw new RuntimeException("Must by only one result");
            }
            return agents.get(0);
        }

        private AID createNewProfilingAgent() {
            try {
                PlatformController container = dispatcherAgent.getContainerController(); // get a container controller for creating new agents

                String localName = "profiling-agent-" + profilingRequest.getClientId();
                String[] args = {profilingRequest.getLocalisation()};
                AgentController agent = container.createNewAgent(localName, "sag.rekomendacje.ProfilingAgent", args);
                agent.start();

                AID profilingId = new AID(localName, AID.ISLOCALNAME);

                System.out.println(dispatcherAgent.getAID().getName() + " stworzyl agenta dla klienta o id: " +
                    profilingRequest.getClientId());

                return profilingId;
            } catch (Exception e) {
                System.err.println("Exception while adding profiling agent: " + e);
                e.printStackTrace();
            }
            return null;
        }

        private void sendRequestMessage(AID profilingAgentId) {
            String conversationId = "new-request";

            ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
            msg.setContent(profilingRequest.getRequest() + ";" + profilingRequest.getLocalisation());
            msg.setConversationId(conversationId);
            msg.addReceiver(profilingAgentId);
            msg.setReplyWith(conversationId + System.currentTimeMillis()); // Unique value
            myAgent.send(msg);
            messageTemplate = MessageTemplate.and(MessageTemplate.MatchConversationId(conversationId),
                MessageTemplate.MatchInReplyTo(msg.getReplyWith()));
            state = State.RECEIVING;
        }

        private void receiveResponseMessage() {
            ACLMessage reply = myAgent.receive(messageTemplate);
            if (reply != null) {
                String receiverId = reply.getSender().getLocalName().substring("profiling-agent-".length());
                if (reply.getPerformative() == ACLMessage.PROPOSE) {
                    System.out.println(dispatcherAgent.getAID().getName() +
                        " przyjal rekomendacje dla: " + receiverId + " o tresci: " + reply.getContent());
                } else {
                    System.out.println(dispatcherAgent.getAID().getName() +
                        " nie otrzymal rekomendacji dla: " + receiverId);
                }
                state = State.DONE;
            } else {
                block();
            }
        }

        @Override
        public boolean done() {
            return state == State.DONE;
        }

        private enum State {
            SENDING, RECEIVING, DONE
        }
    }
}
