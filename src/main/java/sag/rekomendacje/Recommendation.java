package sag.rekomendacje;

import java.io.Serializable;

/**
 * User: Bartlomiej Stefanski
 */
class Recommendation implements Serializable, Cloneable {
    String recommendation;
    Double weight;

    Recommendation(String recommendation) {
        this(recommendation, 1.0);
    }

    Recommendation(String recommendation, Double weight) {
        this.recommendation = recommendation;
        this.weight = weight;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getVal() {
        return recommendation;
    }

    @Override
    public String toString() {
        return "[" + getVal() + ", " + getWeight() + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Recommendation)) {
            return false;
        }

        Recommendation that = (Recommendation) o;

        if (!recommendation.equals(that.recommendation)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return recommendation.hashCode();
    }

    public void amplify() {
        weight += 1;
    }

    @Override
    protected Object clone() {
        Recommendation clone = null;
        try {
            clone = (Recommendation) super.clone();

        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e); // won't happen
        }
        return clone;
    }

}

