package sag.rekomendacje;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;

import java.util.*;

/**
 * User: Bartlomiej Stefanski
 */
class RecommendationCalculator {
    List<Recommendation> recommendations;
    private List<Recommendation> neighborsRecommendations;

    public RecommendationCalculator(String request, List<Recommendation> recommendations, List<Recommendation> neighborsRecommendations) {
        this.recommendations = new ArrayList<>();
        if (recommendations != null) {
            for (Recommendation r : recommendations) {
                if (!r.getVal().equals(request)) {
                    this.recommendations.add((Recommendation) r.clone());
                }
            }
        }

        this.neighborsRecommendations = new ArrayList<>();
        if (neighborsRecommendations != null) {
            for (Recommendation r : neighborsRecommendations) {
                if (!r.getVal().equals(request)) {
                    this.neighborsRecommendations.add((Recommendation) r.clone());
                }
            }
        }
    }

    public List<Recommendation> calculate() {
        Ordering<Recommendation> recommendationOrdering = getRecommendationOrdering();
        neighborsRecommendations = Lists.transform(neighborsRecommendations, getNeigbhborsRecommendationsDivider());
        Iterable<Recommendation> allRec = Iterables.concat(recommendations, neighborsRecommendations);
        List<Recommendation> allUniqueRec = joinSameRecommendations(allRec);
        Collections.shuffle(allUniqueRec);
        return recommendationOrdering.greatestOf(Lists.newArrayList(allUniqueRec), 5);
    }

    private List<Recommendation> joinSameRecommendations(Iterable<Recommendation> allRec) {
        Map<String, Recommendation> allUniqueRec = Maps.newHashMap();
        for (Recommendation r : allRec) {
            if (allUniqueRec.containsKey(r.getVal())) {
                allUniqueRec.get(r.getVal()).setWeight(allUniqueRec.get(r.getVal()).getWeight() + r.getWeight());
            } else {
                allUniqueRec.put(r.getVal(), r);
            }
        }
        return new ArrayList<>(allUniqueRec.values());
    }

    private Function<Recommendation, Recommendation> getNeigbhborsRecommendationsDivider() {
        return new Function<Recommendation, Recommendation>() {
                    public Recommendation apply(Recommendation recommendation) {
                        recommendation.setWeight(recommendation.getWeight()/10);
                        return recommendation;
                    }
                };
    }

    private Ordering<Recommendation> getRecommendationOrdering() {
        return Ordering.natural().nullsLast().onResultOf(
                new Function<Recommendation, Comparable>() {
                    @Override
                    public Comparable apply(Recommendation recommendation) {
                        return recommendation.getWeight();
                    }
                });
    }
}
