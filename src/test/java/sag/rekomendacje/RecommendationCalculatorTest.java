package sag.rekomendacje;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;

public class RecommendationCalculatorTest {
    private RecommendationCalculator calculator;
    private ArrayList<Recommendation> recommendations;
    private ArrayList<Recommendation> neighborRecommendations;

    @Before
    public void setUp() throws Exception {
        recommendations = Lists.newArrayList(
            new Recommendation("czarny", 3.0),
            new Recommendation("czerwony", 2.0),
            new Recommendation("zielony", 2.0),
            new Recommendation("zolty", 2.0),
            new Recommendation("niebieski", 2.0),
            new Recommendation("brazowy", 1.0),
            new Recommendation("pomaranczowy", 1.0),
            new Recommendation("rozowy", 1.0),
            new Recommendation("fioletowy", 1.0),
            new Recommendation("szary", 1.0));

        neighborRecommendations = Lists.newArrayList(
            new Recommendation("czarny"),
            new Recommendation("czerwony"),
            new Recommendation("zielony"),
            new Recommendation("zolty"),
            new Recommendation("niebieski"),
            new Recommendation("brazowy"),
            new Recommendation("pomaranczowy"),
            new Recommendation("rozowy"),
            new Recommendation("fioletowy"),
            new Recommendation("szary"));

        calculator = new RecommendationCalculator("bialy", recommendations, neighborRecommendations);
    }

    @Test
    public void shouldNotModifyOriginRecommendations() throws Exception {
        // When
        calculator.calculate();

        // Then
        assertThat(recommendations).usingElementComparator(new Comparator<Recommendation>() {
            @Override
            public int compare(Recommendation o1, Recommendation o2) {
                return ComparisonChain.start().compare(o1.getWeight(), o2.getWeight()).result();

            }
        }).contains(
            new Recommendation("czarny"),
            new Recommendation("czerwony"),
            new Recommendation("zielony"),
            new Recommendation("zolty"),
            new Recommendation("niebieski"),
            new Recommendation("brazowy"),
            new Recommendation("pomaranczowy"),
            new Recommendation("rozowy"),
            new Recommendation("fioletowy"),
            new Recommendation("szary"));
    }

    @Test
    public void shouldCalculateRecommendations() throws Exception {
        // When
        List<Recommendation> result = calculator.calculate();

        // Then
        assertThat(result).containsOnly(
            new Recommendation("czarny"),
            new Recommendation("czerwony"),
            new Recommendation("zielony"),
            new Recommendation("zolty"),
            new Recommendation("niebieski"));
    }

    @Test
    public void shouldReturnEmptyListIfAllIsEmpty() throws Exception {
        // Given
        RecommendationCalculator calculator =
            new RecommendationCalculator("", new ArrayList<Recommendation>(), new ArrayList<Recommendation>());

        // When
        List<Recommendation> result = calculator.calculate();

        // Then
        assertThat(result).isNotNull().isEmpty();
    }

    @Test
    public void shouldReturnNeighborsRecIfRecommendationsAreEmpty() throws Exception {
        // Given
        ArrayList<Recommendation> neighborsRecommendations = new ArrayList<>();
        neighborsRecommendations.add(new Recommendation("a"));
        neighborsRecommendations.add(new Recommendation("b"));
        neighborsRecommendations.add(new Recommendation("c"));
        RecommendationCalculator calculator =
            new RecommendationCalculator("bialy", new ArrayList<Recommendation>(), neighborsRecommendations);

        // When
        List<Recommendation> result = calculator.calculate();

        // Then
        assertThat(result).containsOnly(new Recommendation("a"), new Recommendation("b"), new Recommendation("c"));
    }
}
